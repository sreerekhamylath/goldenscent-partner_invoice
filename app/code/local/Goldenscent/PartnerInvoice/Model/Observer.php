<?php

/**
 * Goldenscent Test for Partner invoices - Observer file
 *
 * @category  Goldenscent
 * @package   Goldenscent_PartnerInvoice
 * @author    Sreerekha Mylath <sreerekhamylath@gmail.com>
 */
class Goldenscent_PartnerInvoice_Model_Observer
{
    /**
     * Create cookie
     * @param Varien_Event_Observer $observer Magento passes a Varien_Event_Observer object as the first parameter of dispatched events
     */
    public function createPartnerCookie(Varien_Event_Observer $observer)
    {
        if (Mage::app()->getRequest()->getParam('partner')) {
            $partnerName = Mage::app()->getRequest()->getParam('partner');
            $partnerCookie = Mage::getModel('core/cookie');
            $partnerCookie->set('partner', $partnerName, 86400);
        }
    }

    /**
     * Create multiple invoices and shipments based on cookie when order is placed
     * @param $observer
     * @return $this
     */
    public function createPartnerInvoiceBasedOnCookie($observer)
    {
        $partner = Mage::getModel('core/cookie')->get('partner');
        if ($partner) {
            $order = $observer->getEvent()->getOrder();
            $orders = Mage::getModel('sales/order_invoice')->getCollection()
                ->addAttributeToFilter('order_id', array('eq' => $order->getId()));
            $orders->getSelect()->limit(1);
            if ((int)$orders->count() !== 0) {
                return $this;
            }
            if ($order->getState() == Mage_Sales_Model_Order::STATE_NEW) {
                try {
                    if (!$order->canInvoice()) {
                        $order->addStatusHistoryComment('Goldenscent_PartnerInvoice: Order cannot be invoiced.', false);
                        $order->save();
                    }
                    $qty = $this->getInvoiceItems($order); //Get Split items for multiple invoice and shipment
                    //START Handle Invoice
                    foreach ($qty as $item) {
                        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice($item);
                        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
                        $invoice->register();

                        $invoice->getOrder()->setCustomerNoteNotify(false);
                        $invoice->getOrder()->setIsInProcess(true);
                        $order->addStatusHistoryComment('Automatically INVOICED by Goldenscent_PartnerInvoice.', false);
                        $transactionSave = Mage::getModel('core/resource_transaction')
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder());
                        $transactionSave->save();
                        //END Handle Invoice

                        //START Handle Shipment
                        $shipment = $order->prepareShipment($item);
                        $shipment->register();

                        $order->setIsInProcess(true);
                        $order->addStatusHistoryComment('Automatically SHIPPED by Goldenscent_PartnerInvoice.', false);
                        $transactionSave = Mage::getModel('core/resource_transaction')
                            ->addObject($shipment)
                            ->addObject($shipment->getOrder());
                        $transactionSave->save();
                        //END Handle Shipment
                    }
                } catch (Exception $e) {
                    $order->addStatusHistoryComment('Goldenscent_PartnerInvoice: Exception occurred during automaticallyInvoiceShipCompleteOrder action. Exception message: ' . $e->getMessage(), false);
                    $order->save();
                }
            }
        }
        return $this;
    }

    /**
     * Creating the split items based on requirement
     * @param $order
     * @return array
     */
    public function getInvoiceItems($order)
    {
        $items = [];
        foreach ($order->getAllVisibleItems() as $orderItem) {
            $items[$orderItem->getItemId()] = $orderItem->getQtyOrdered();
        }
        $half = ceil(array_sum($items) / 2);
        $splitItem1 = $splitItem2 = [];
        foreach ($items as $key => $value) {
            if (array_sum($splitItem2) >= array_sum($splitItem1) && ($value <= $half)) {
                $splitItem1[$key] = (array_sum($splitItem1) + $value <= $half) ? $value : array_sum($splitItem1) + $value - $half;
                $splitItem2[$key] = (array_sum($splitItem1) + $value <= $half) ? 0 : $value - $splitItem1[$key];
            } else {
                $splitItem2[$key] = ($value >= $half) ? $half : $value;
                $splitItem1[$key] = ($value - $half > 0) ? $value - $half : 0;
            }
        }
        $splitItems = [array_filter($splitItem1), array_filter($splitItem2)];
        return $splitItems;
    }

    /**
     * Save Partner Data to Order From cookie
     * @param $observer
     * @return $this
     */
    public function savePartnerData($observer)
    {
        $event = $observer->getEvent();
        $order = $event->getOrder();
        $partner = Mage::getModel('core/cookie')->get('partner');
        if ($partner) {
            $order->setPartner($partner);
        }
        return $this;
    }

    /**
     * Joins tables for adding partner column to Mage_Adminhtml_Block_Sales_Order_Grid
     * @param $observer
     */
    public function salesOrderGridCollectionLoadBefore($observer)
    {
        $collection = $observer->getOrderGridCollection();
        $select = $collection->getSelect();
        $select->joinLeft(array('order' => $collection->getTable('sales/order')), 'order.entity_id=main_table.entity_id', array('partner' => 'partner'));
        $select->group('main_table.entity_id');
    }
}
