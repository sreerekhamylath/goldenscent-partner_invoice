<?php
/**
 * Goldenscent Test for Partner invoices - Adding partner column to Quote and Order Table
 *
 * @category  Goldenscent
 * @package   Goldenscent_PartnerInvoice
 * @author    Sreerekha Mylath <sreerekhamylath@gmail.com>
 */
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order", "partner", array("type" => "varchar"));
$installer->addAttribute("quote", "partner", array("type" => "varchar"));
$installer->endSetup();