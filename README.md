# README #

### What is this repository for? ###
Tech test for Senior Magento Developer
Magento 1.9 - Partner invoices

Task formulation

Imagine a webshop build in Magento 1.9, that sells physical items. The items are actually sold by different physical stores, but there is also a main warehouse. 

Any physical store can do their own marketing activities and can send traffic to the site. Based on the partnership contract, when a customer come referred by a physical store, the order will be shared, meaning that the store will ship and invoice half of the products, and the main warehouse will ship and invoice the other half. 

We need to do an implementation that allows the following:

Customers will arrive in the site following a specific link with a parameter identifying who is sending the traffic. The parameter will be ?partner={ANY STRING}
We will keep the partner name in a cookie so we know that user came from that partner. The lifetime of the cookie will be 24h. 
When an order is placed, we will check the cookie information to see if the customer comes from any partner.
If the customer comes from a partner, then
We will generate 2 shipments, each of them will have half of the items 
We will generate 2 invoices, one per shipment, so every invoice will include the items that are together in the same shipment. 
If the number of items is odd then one shipment/invoice will have one item more than the other. 
If the number of items is 1 then there will be only one shipment and one invoice
In Magento admin, the Order grid will show a new column called “Partner name”


